import sum from "./sum.helper";

describe("sum()", () => {
  describe("The sum helper can sum", () => {
    const a = 1;
    const b = 2;
    const result = sum(a, b);

    it("Has the correct result", () => {
      expect(result).toEqual(3);
    });
  });
});
