import React from "react";
import ReactDom from "react-dom";
import Index from "~pages/Index/Index";

const App = () => <Index />;

ReactDom.render(<App />, document.getElementById("app"));
