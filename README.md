# Typescript react template

Project template with Typescript and React

## What do I need

You need at least the following

- gnu make
- docker / dockercompose

## How do I start

1. Fork the project
1. Run `make init` to initialize the project.
1. Run `make start` to start the app container
1. Run `make` to see all other available make commands
